export WUSER=/mnt/c/Users/Daemonecles

alias aliasconfig='vim ~/src/dotfiles/bash_aliases'
alias zshconfig='vim ~/src/dotfiles/zshrc'
alias bashconfig='vim ~/src/dotfiles/bashrc'

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias dev='cd /code/dev/'

alias cmd='/mnt/c/Windows/System32/cmd.exe /C'
alias not='cmd start .'
alias code='cmd code $1'

alias windocs='cd $WUSER/Documents'
alias windown='cd $WUSER/Downloads'
alias ffmpeg='$WUSER/Documents/ShareX/Tools/ffmpeg.exe'
